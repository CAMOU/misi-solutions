const submitBtn = document.getElementById("contact-submit");

submitBtn.addEventListener("click", () => {
    const firstName = document.getElementById("contact-firstName")
    const lastName = document.getElementById("contact-lastName")
    const email = document.getElementById("contact-email")
    const service = document.getElementById("contact-service")
    const message = document.getElementById("contact-message")

    // validation check
    if(firstName.classList.contains("valid") && lastName.classList.contains("valid") && email.classList.contains("valid") && service.classList.contains("valid") && message.classList.contains("valid")) {
        // send mail
        emailjs.sendForm('contact_service', 'template_lclgego', '#contact-form-html');

        // submit animation
        if(!document.querySelector(".contact-form-button").classList.contains("sending")) {
            document.querySelector(".contact-form-button").classList.remove("sending")
            setTimeout(() => { document.querySelector(".contact-form-button").classList.add("sending") }, 100)
            setTimeout(() => { document.querySelector(".contact-form-button").classList.remove("sending") }, 4500)

            setTimeout(() => { document.querySelector(".contact-form-button #contact-submit-sending").classList.add("sending-icon") }, 800)
            setTimeout(() => { document.querySelector(".contact-form-button #contact-submit-sending").classList.remove("sending-icon") }, 1600)

            setTimeout(() => { document.querySelector(".contact-form-button #contact-submit-checkmark").classList.add("sending") }, 1800)
            setTimeout(() => { document.querySelector(".contact-form-button #contact-submit-checkmark").classList.remove("sending") }, 3600)
        }

        // clear contact form
        setTimeout(() => {
            document.getElementById("contact-form-html").reset()
            firstName.classList.remove("valid")
            lastName.classList.remove("valid")
            email.classList.remove("valid")
            service.classList.remove("valid")
            message.classList.remove("valid")
        }, 3600)
    } else {
        submitBtn.classList.remove("shake")
        setTimeout(() => { submitBtn.classList.add("shake") }, 100)
    }
})

function validation(id, type) {
    const element = document.getElementById(id)

    switch(type) {
        case "EMAIL":
            const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

            if (element.value !== '' && emailRegex.test(element.value.toLowerCase())) {
                element.classList.remove("invalid")
                element.classList.add("valid")
            } else {
                element.classList.add("invalid")
                element.classList.remove("valid")
            }

            break;
        case "SERVICE":
            const serviceRegex = /^[a-zA-Z\s]*$/

            if(element.value !== '' && element.value.length > 5 && element.value.length < 25 && serviceRegex.test(element.value)) {
                element.classList.remove("invalid")
                element.classList.add("valid")
            } else {
                element.classList.add("invalid")
                element.classList.remove("valid")
            }

            break;
        case "MESSAGE":
            if(element.value !== '' && element.value.length > 10 && element.value.length < 450) {
                element.classList.remove("invalid")
                element.classList.add("valid")
            } else {
                element.classList.add("invalid")
                element.classList.remove("valid")
            }

            break;
        default:
            const defaultRegex = /^[a-zA-Z]+$/

            if(element.value !== '' && element.value.length > 2 && element.value.length < 20 && defaultRegex.test(element.value)) {
                element.classList.remove("invalid")
                element.classList.add("valid")
            } else {
                element.classList.add("invalid")
                element.classList.remove("valid")
            }

            break;
    }
}