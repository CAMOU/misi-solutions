// Load Analytics Code dynamically
function loadAnalytics() {
    window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
    ga('create', 'UA-180687103-1', 'auto');
    ga('set', 'anonymizeIp', true);
    ga('send', 'pageview');
    const gascript = document.createElement("script");
    gascript.async = true;
    gascript.src = "https://www.google-analytics.com/analytics.js";
    document.getElementsByTagName("head")[0].appendChild(gascript, document.getElementsByTagName("head")[0]);
}

if (document.cookie.split(';').filter(function(item) {
    return item.indexOf('cookieconsent_status=allow') >= 0
}).length) {
    if (navigator.doNotTrack != 1 && navigator.doNotTrack != "yes" && window.doNotTrack != 1 && navigator.msDoNotTrack != 1) {
        loadAnalytics();
    }
}

window.addEventListener("load", function() {
    window.cookieconsent.initialise({
        "palette": {
            "popup": {
                "background": "#f5f5f5"
            },
            "button": {
                "background": "#ef007d"
            }
        },
        "theme": "classic",
        "type": "opt-in",
        "content": {
            "href": "https://www.misi.solutions/privacy-policy"
        },
        onStatusChange: function(status, chosenBefore) {
            const type = this.options.type;
            const didConsent = this.hasConsented();
            if (type === 'opt-in' && didConsent) {
                // enable cookies
                loadAnalytics();
            }
        }
    });
});