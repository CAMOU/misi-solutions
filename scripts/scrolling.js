const servicesBtn = document.getElementById("nav-services")
const portfolioBtn = document.getElementById("nav-portfolio")
const contactBtn = document.querySelectorAll(".contact-btn")
const pageUpBtn = document.getElementById("page-up")
const scrollDown = document.getElementById("scroll-down")

const fileName = window.location.pathname.split("/").pop()

window.onload = () => {
    if(document.cookie != null && document.cookie !== "" && document.cookie !== "goto=") {
        document.getElementById(document.cookie.substring(5)).scrollIntoView(true)
        document.cookie = "goto=;"
    }
}

servicesBtn.addEventListener("click", () => {
    if(fileName !== "") {
        document.cookie = "goto=services-view"
        window.open("/", "_self")
        return
    }

    document.getElementById("services-view").scrollIntoView(true)
})

portfolioBtn.addEventListener("click", () => {
    if(fileName !== "") {
        document.cookie = "goto=portfolio-view"
        window.open("/", "_self")
        return
    }

    document.getElementById("portfolio-view").scrollIntoView(true)
})

contactBtn.forEach(button => {
   button.addEventListener("click", () => {
       if(fileName !== "") {
           document.cookie = "goto=contact-view"
           window.open("/", "_self")
           return
       }

       document.getElementById("contact-view").scrollIntoView(true)
   })
})

if(fileName === '') {
    scrollDown.addEventListener("click", () => {
        document.getElementById("services-view").scrollIntoView(true)
    })
}

/* SCROLL UP BUTTON */
window.onscroll = () => {
    if(document.body.scrollTop > 400 || document.documentElement.scrollTop > 400) {
        document.getElementById("page-up").style.opacity = 1
        document.getElementById("page-up").style.cursor = "pointer"
    } else {
        document.getElementById("page-up").style.opacity = 0
        document.getElementById("page-up").style.cursor = "default"
        document.getElementById("page-up").style.transform = "translateY(0)"
    }
}

pageUpBtn.addEventListener("click", () => {
    window.scrollTo(0, 0)
    document.getElementById("page-up").style.transform = "translateY(-2em)"
})