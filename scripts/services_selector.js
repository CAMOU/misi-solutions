const webDevelopmentBtn = document.getElementById("web-development")
const gameDevelopmentBtn = document.getElementById("game-development")
const systemAdministrationBtn = document.getElementById("system-administration")

const webDevelopmentInfo = document.getElementById("web-development-info")
const gameDevelopmentInfo = document.getElementById("game-development-info")
const systemAdministrationInfo = document.getElementById("system-administration-info")

const servicesInfoboxImage = document.getElementById("services-infobox-image");
let currentInfo = webDevelopmentInfo

webDevelopmentBtn.addEventListener('click', () => {
   webDevelopmentBtn.classList.add("active");
   gameDevelopmentBtn.classList.remove("active");
   systemAdministrationBtn.classList.remove("active");

   if(currentInfo === webDevelopmentInfo) return;

   servicesInfoboxImage.classList.add("fade-out");
   currentInfo.classList.add("fade-out");
   setTimeout(() => {
       currentInfo.classList.add("hidden");
       currentInfo.classList.remove("fade-out")
       servicesInfoboxImage.classList.remove("fade-out");
       servicesInfoboxImage.classList.add("fade-in");
       webDevelopmentInfo.classList.add("fade-in");
       webDevelopmentInfo.classList.remove("hidden");

       setTimeout(() => { webDevelopmentInfo.classList.remove("fade-in"); servicesInfoboxImage.classList.remove("fade-in"); }, 500)

       servicesInfoboxImage.src = "media/service-webdevelopment.png";
       currentInfo = webDevelopmentInfo;
   }, 300);
});

gameDevelopmentBtn.addEventListener('click', () => {
    webDevelopmentBtn.classList.remove("active");
    gameDevelopmentBtn.classList.add("active");
    systemAdministrationBtn.classList.remove("active");

    if(currentInfo === gameDevelopmentInfo) return;

    servicesInfoboxImage.classList.add("fade-out");
    currentInfo.classList.add("fade-out");
    setTimeout(() => {
        currentInfo.classList.add("hidden");
        currentInfo.classList.remove("fade-out")
        servicesInfoboxImage.classList.remove("fade-out");
        servicesInfoboxImage.classList.add("fade-in");
        gameDevelopmentInfo.classList.add("fade-in");
        gameDevelopmentInfo.classList.remove("hidden");

        setTimeout(() => { gameDevelopmentInfo.classList.remove("fade-in"); servicesInfoboxImage.classList.remove("fade-in"); }, 500)

        servicesInfoboxImage.src = "media/service-gamedevelopment.png"
        currentInfo = gameDevelopmentInfo;
    }, 300);
});

systemAdministrationBtn.addEventListener('click', () => {
    webDevelopmentBtn.classList.remove("active");
    gameDevelopmentBtn.classList.remove("active");
    systemAdministrationBtn.classList.add("active");

    if(currentInfo === systemAdministrationInfo) return;

    servicesInfoboxImage.classList.add("fade-out");
    currentInfo.classList.add("fade-out");
    setTimeout(() => {
        currentInfo.classList.add("hidden");
        currentInfo.classList.remove("fade-out")
        servicesInfoboxImage.classList.remove("fade-out");
        servicesInfoboxImage.classList.add("fade-in");
        systemAdministrationInfo.classList.add("fade-in");
        systemAdministrationInfo.classList.remove("hidden");

        setTimeout(() => { systemAdministrationInfo.classList.remove("fade-in"); servicesInfoboxImage.classList.remove("fade-in"); }, 500)

        servicesInfoboxImage.src = "media/service-systemadministration.png"
        currentInfo = systemAdministrationInfo;
    }, 300);
});